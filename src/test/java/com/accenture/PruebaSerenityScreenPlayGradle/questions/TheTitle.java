package com.accenture.PruebaSerenityScreenPlayGradle.questions;

import com.accenture.PruebaSerenityScreenPlayGradle.ui.YouTubeVideo;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class TheTitle implements Question<String>{
	
	YouTubeVideo youTubeVideo;
	
	public String answeredBy(Actor actor) {
		return Text.of(youTubeVideo.titulo).viewedBy(actor).asString();
	}

	public static TheTitle ofVideo() {
		return new TheTitle();
	}

}


