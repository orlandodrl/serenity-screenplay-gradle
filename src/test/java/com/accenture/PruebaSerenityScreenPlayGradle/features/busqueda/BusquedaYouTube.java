package com.accenture.PruebaSerenityScreenPlayGradle.features.busqueda;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import com.accenture.PruebaSerenityScreenPlayGradle.questions.TheTitle;
import com.accenture.PruebaSerenityScreenPlayGradle.task.ChooseTheVideo;
import com.accenture.PruebaSerenityScreenPlayGradle.task.OpenTheBrowser;
import com.accenture.PruebaSerenityScreenPlayGradle.task.Search;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.*;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.containsString;

@RunWith(SerenityRunner.class)
public class BusquedaYouTube {
	
	 Actor anna = Actor.named("Anna");
	 
	 @Managed()
	 public WebDriver herBrowser;
	 
	 @Steps
	 OpenTheBrowser openTheBrowser;
	 
	 @Before
     public void annaCanBrowseTheWeb() {
        anna.can(BrowseTheWeb.with(herBrowser));
     }
	 
	 @Test
     public void the_title_of_the_song_on_youtube_must_contain_the_search_term() {

        givenThat(anna).wasAbleTo(openTheBrowser);

        when(anna).attemptsTo(
        		Search.TheTerm("Thunderclouds"),
        		ChooseTheVideo.numberTwo()
        		);
        
        then(anna).should(eventually(seeThat(TheTitle.ofVideo(), containsString("Thunderclouds"))));

     }

}
