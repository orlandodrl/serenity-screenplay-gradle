package com.accenture.PruebaSerenityScreenPlayGradle.ui;

import java.util.List;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;


public class YouTubeVideos extends PageObject {
	
	public List<WebElementFacade> listaVideos() {
		List<WebElementFacade> listaVideos = findAll(By.xpath("//a[@class=\"yt-simple-endpoint style-scope ytd-video-renderer\"]"));
		return listaVideos;
	}
	
}


