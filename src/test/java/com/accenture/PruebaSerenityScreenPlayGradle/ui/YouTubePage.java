package com.accenture.PruebaSerenityScreenPlayGradle.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.youtube.com/")
public class YouTubePage extends PageObject {

	public Target buscador = Target.the("Buscador de YouTube").locatedBy("//*[@id=\"search\"]//input");

}


