package com.accenture.PruebaSerenityScreenPlayGradle.task;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import org.openqa.selenium.Keys;
import com.accenture.PruebaSerenityScreenPlayGradle.ui.YouTubePage;

public class Search implements Task {
	
	YouTubePage youTubePage;
	String termino;
	
	public Search(String termino) {
		this.termino = termino;
	}
	
	@Step("{0} busca #termino en la barra de busqueda de YouTube")
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(
					Enter.theValue(termino)
					.into(youTubePage.buscador)
					.thenHit(Keys.ENTER)
				);
	}

	public static Search TheTerm(String termino) {
		return instrumented(Search.class, termino);
	}

}


