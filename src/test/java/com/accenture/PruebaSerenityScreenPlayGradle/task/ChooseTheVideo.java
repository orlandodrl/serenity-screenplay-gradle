package com.accenture.PruebaSerenityScreenPlayGradle.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import com.accenture.PruebaSerenityScreenPlayGradle.ui.YouTubeVideos;

public class ChooseTheVideo implements Task {
	
	YouTubeVideos youTubeVideos;
	
	@Step("{0} selecciona el segundo video de la lista")
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(
				Click.on(youTubeVideos.listaVideos().get(2))
				);
		
	}

	public static ChooseTheVideo numberTwo() {
		return instrumented(ChooseTheVideo.class);
	}
	
}


