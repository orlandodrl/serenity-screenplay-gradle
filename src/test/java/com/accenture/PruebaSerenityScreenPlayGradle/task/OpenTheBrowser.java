package com.accenture.PruebaSerenityScreenPlayGradle.task;

import com.accenture.PruebaSerenityScreenPlayGradle.ui.YouTubePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

public class OpenTheBrowser implements Task {
	
	YouTubePage youTubePage;
	
	@Step("{0} abre el navegador en la pagina de YouTube")
	public <T extends Actor> void performAs(T actor) {
		actor.wasAbleTo(
					Open.browserOn(youTubePage)
				);
	}

}


